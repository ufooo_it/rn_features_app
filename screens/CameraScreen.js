import React, {useState} from 'react';
import {Button, Text, View, StyleSheet, Image, ScrollView} from 'react-native';
import ImageCropPicker from 'react-native-image-crop-picker';
import VideoPlayer from 'react-native-video-player';

const CameraScreen = (props) => {
  const [imageUrl, setImageUrl] = useState();
  const ImageOptions = {
    width: 400,
    height: 200,
    cropping: true,
  };

  const takeImageHandler = () => {
    ImageCropPicker.openCamera(ImageOptions)
      .then((image) => {
        setImageUrl(image.path);
      })
      .catch((err) => console.log(err));
  };
  const pickImageHandler = () => {
    ImageCropPicker.openPicker(ImageOptions)
      .then((image) => {
        setImageUrl(image.path);
      })
      .catch((err) => console.log(err));
  };

  return (
    <ScrollView>
      <View style={styles.imagePicker}>
        <View style={styles.imagePreview}>
          {imageUrl ? (
            <Image style={styles.image} source={{uri: imageUrl}} />
          ) : (
            <Text>No image picked yet.</Text>
          )}
        </View>
        <View style={{flexDirection: 'row', marginBottom: 10}}>
          <Button title="Take Image" onPress={takeImageHandler} />
          <View style={{width: 20}} />
          <Button title="Pick Image" onPress={pickImageHandler} />
        </View>
        <View style={styles.videoPreview}>
          <VideoPlayer
            video={{
              uri:
                'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
            }}
            resizeMode="contain"
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  imagePicker: {
    alignItems: 'center',
    margin: 10,
  },
  imagePreview: {
    width: '100%',
    height: 200,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ccc',
    borderWidth: 1,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  videoPreview: {
    width: '100%',
    marginBottom: 10,
    borderColor: '#ccc',
    borderWidth: 1,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default CameraScreen;
