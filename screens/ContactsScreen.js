import React, {useState} from 'react';
import {PermissionsAndroid, View, Text, Button} from 'react-native';
import Contacts from 'react-native-contacts';
import {FlatList} from 'react-native-gesture-handler';

const ContactsScreen = (props) => {
  const [contactsList, setContactsList] = useState([]);

  const requestContactsPermission = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: 'Contacts',
      message: 'This app would like to view your contacts.',
      buttonPositive: 'Please accept bare mortal',
    })
      .then(() => {
        Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
          } else {
            setContactsList(contacts);
          }
        });
      })
      .catch((err) => console.log(err));
  };

  return (
    <View style={{padding: 10}}>
      <Button title="Show Contact" onPress={requestContactsPermission} />
      {contactsList.length > 0 ? (
        contactsList.map((contact, index) => {
        return <Text key={index}>{contact.displayName} - {contact.phoneNumbers[0].number}</Text>;
        })
      ) : (
        <></>
      )}
    </View>
  );
};

export default ContactsScreen;
