import React from 'react';
import {FlatList, Text, View, StyleSheet, Pressable} from 'react-native';

const HomeScreen = (props) => {
  props.navigation.addListener('beforeRemove', (e) => {
    e.preventDefault();
  });

  const screenData = [
    {name: 'Location', title: 'Location'},
    {name: 'Camera', title: 'Camera'},
    {name: 'FileSystem', title: 'File System'},
    {name: 'Notification', title: 'Notification'},
    {name: 'Sms', title: 'SMS'},
    {name: 'Contacts', title: 'Contacts'},
  ];

  const _renderItem = ({item}) => {
    return (
      <Pressable
        onPress={() => {
          props.navigation.navigate(item.name);
        }}>
        <View style={styles.item}>
          <Text>{item.title}</Text>
        </View>
      </Pressable>
    );
  };

  return (
    <FlatList
      data={screenData}
      keyExtractor={(item) => item.name}
      renderItem={_renderItem}
    />
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 20,
    borderBottomWidth: 1,
  },
});

export default HomeScreen;
