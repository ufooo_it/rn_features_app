import React, {useState} from 'react';
import {Button, Text, View, StyleSheet} from 'react-native';
import Geolocation from '@react-native-community/geolocation';

const LocationScreen = (props) => {
  const [locationInfo, setLocationInfo] = useState();
  Geolocation.setRNConfiguration({
    skipPermissionRequests: false,
    authorizationLevel: 'always',
  });

  const locationHandler = () => {
    Geolocation.getCurrentPosition((info) => setLocationInfo(info.coords));
  };

  return (
    <View style={styles.view}>
      <Button title="Get Location" onPress={locationHandler} />
      <Text style={styles.text}>
        Latitude: {locationInfo ? locationInfo.latitude : ''}
      </Text>
      <Text style={styles.text}>
        Longitude: {locationInfo ? locationInfo.longitude : ''}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    margin: 10,
  },
  text: {
    marginHorizontal: 5,
    marginBottom: 5,
  },
});

export default LocationScreen;
