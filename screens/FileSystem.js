import React, {useState} from 'react';
import {View, StyleSheet, Button, Text} from 'react-native';
import DocumentPicker from 'react-native-document-picker';

const FileSystem = (props) => {
  const [files, setFiles] = useState([]);
  const pickFileHandler = async () => {
    const file = await DocumentPicker.pick();
    setFiles([file]);
  };

  const pickFilesHandler = async () => {
    const files = await DocumentPicker.pickMultiple();
    setFiles(files);
  };

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', marginBottom: 10}}>
        <Button title="Pick file" onPress={pickFileHandler} />
        <View style={{width: 20}} />
        <Button title="Pick files" onPress={pickFilesHandler} />
      </View>
      <View>
        <Text>File information: </Text>
        {files.length > 0
          ? files.map((file, index) => {
              return <Text key={index}>file name: {file.name}</Text>;
            })
          : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    margin: 10,
  },
});

export default FileSystem;
